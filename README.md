# Harmonik

A Harmonik egy zenei ízlés alapú társkereső oldal.

## Szükséges szoftverek

Az alkalmazás futtatásához a számítógépen szükség van a `Docker Desktop` nevű alkalmazásra, melyet az alábbi linken lehet letölteni:
[Docker Desktop Download](https://www.docker.com/products/docker-desktop/)

Windows-ra a telepítéshez útmutató itt érhető el (ezen az oldalon körülnézve más operációs rendszerekhez is van segédlet):
[Docker Desktop Windows install](https://docs.docker.com/desktop/install/windows-install/#install-docker-desktop-on-windows)

Windows-on továbbá szükséges a `WSL2` telepítése is:
[WSL2 install](https://learn.microsoft.com/en-us/windows/wsl/install)

Ezenkívül a szolgáltatások megnyitását `Visual Studio 2022`-vel, a kliens forráskódjának böngészését `Visual Studio Code`-dal ajánlom.

## A projekt tartalma

Az alkalmazás forráskódja a `README.md` fájl melletti `harmonik-client` mappa alatt található. A kliensalkalmazás kódja a `Client` mappában van, a szolgáltatások pedig a többi mappában foglalnak helyet. A `Harmonik.sln` fájlt megnyitva a `Visual Studio`-ban az összes szolgáltatást láthatjuk egy helyen.

## Az alkalmazás futtatása

Futtatásra két lehetőség van:
- a `harmonik-client` mappába navigálva, a `docker-compose.yml` fájllal egy szinten egy parancssorból kiadva a `docker compose -f docker-compose.yml -p harmonik up` parancsot, az egyes konténerek elindulnak,
- a `Harmonik.sln` fájlt megnyitva, majd felül a `Docker Compose` gombra kattintva szintén elindul az alkalmazás, így hibakereső (Debug) módban.

Mindkét esetben az alkalmazás betöltése után egy böngészőben (ajánlott Google Chrome) a `http://http://localhost:5083/` címre navigálva eljutunk az alkalmazás főoldalára.

> **_FONTOS:_** Amennyiben szeretné az alkalmazást saját Spotify fiókkal kipróbálni, kérem írjon egy e-mailt nekem a balint.adam111@gmail.com címre a a Spotify-on használt teljes nevével és e mail címével.
Sajnos a Spotify csak előre meghatározott profilokkal engedélyez hasnzálni fejlesztés alatt álló alkalmazásokat, iskolai projekteket pedig csak fejlesztési módban lehet működtetni, így ez a kellemetlen megoldás maradt.
