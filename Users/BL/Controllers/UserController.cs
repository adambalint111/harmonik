﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using RestSharp;
using Users.BL.Model;
using Users.BL.Repositories;

namespace Users.BL.Controllers
{
    [ApiController]
    [Route("/api")]
    public class UserController : ControllerBase
    {
        IUserRepository userRepository;
        RestClient profileClient;
        RestClient matchmakingClient;
        RestClient spotifyManager;

        public UserController(IUserRepository repository)
        {
            userRepository = repository;

            profileClient = new RestClient();
            matchmakingClient = new RestClient();
            spotifyManager = new RestClient();
        }

        [HttpGet("users")]
        public ActionResult<IEnumerable<ApplicationUser>> GetUsers()
        {
            return Ok(userRepository.GetAllUsersAsync());
        }
        
        [HttpGet("users/{id}")]
        public async Task<ActionResult<string>> GetUserById(string id)
        {
            var user = await userRepository.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpGet("emails/{id}")]
        public async Task<ActionResult<string>> GetEmailByUserId(string id)
        {
            var user = await userRepository.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user.Email);
        }

        [HttpPost("users/register")]
        public async Task<IActionResult> RegisterUser([FromBody] RegisterRequestBody registerData)
        {
            string userId = await userRepository.RegisterUserAsync(registerData);

            if (userId != string.Empty)
            {
                RestResponse response = await CreateProfile(userId);
                
                if (!response.IsSuccessful)
                {
                    return BadRequest($"Unable to create profile: {response.ErrorMessage}");
                }

                response = await CreateMatchProfile(userId);

                if (!response.IsSuccessful)
                {
                    return BadRequest($"Unable to create match profile: {response.ErrorMessage}");
                }
            }
            
            return userId != string.Empty ? Ok(userId) : BadRequest($"Error during registration");
        }

        [HttpPost("users/login")]
        public async Task<IActionResult> LoginUser([FromBody] LoginRequestBody loginData)
        {
            AuthenticationResult result = await userRepository.LoginUserAsync(loginData);

            return result != null ? Ok(result) : Unauthorized();
        }

        [HttpPut("users/saveportfolio")]
        public async Task<IActionResult> SaveSpotifyPortfolio([FromBody] SavePortfolioRequestBody saveData)
        {
            var user = await userRepository.GetUserById(saveData.userId);
            
            if (user == null)
            {
                return NotFound();
            }

            user.SpotifyPortfolioId = saveData.porftolioId;
            int changes = await userRepository.SaveChangesAsync();

            if (changes > 0)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpGet("auth")]
        public IActionResult GetAuth()
        {
            // Preflight requests are allowed
            if ((string)Request.Headers["X-Forwarded-Method"] == "OPTIONS")
            {
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Method", "POST, GET, OPTIONS, DELETE, PUT");
                return Ok();
            }

            var uri = (string)Request.Headers["X-Forwarded-Uri"].ToString() ?? "";

            if (uri != "" && (uri.Contains("api/users/login") || uri.Contains("api/users/register")))
            {
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Method", "POST, GET, OPTIONS, DELETE, PUT");
                return Ok();
            }

            var token = Request.Headers["Authorize"].ToString() ?? "";
            if (token.Length <= "Bearer ".Length)
            {
                return Unauthorized();
            }
            string tokenString = ((string)token)?.Substring("Bearer ".Length) ?? "";

            if (userRepository.ValidateToken(tokenString))
            {
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Method", "POST, GET, OPTIONS, DELETE, PUT");
                return Ok();
            }

            return Unauthorized();
        }

        // For Tests only
        [HttpGet("populate")]
        public async Task<IActionResult> PopulateDB()
        {
            List<RegisterRequestBody> mockUsers = new List<RegisterRequestBody>
            {
                new RegisterRequestBody
                {
                    Email = "a@a.com",
                    UserName = "aaa",
                    Password = "A1!_Pass",
                },
                new RegisterRequestBody
                {
                    Email = "b@a.com",
                    UserName = "bbb",
                    Password = "A1!_Pass",
                },
                new RegisterRequestBody
                {
                    Email = "c@a.com",
                    UserName = "ccc",
                    Password = "A1!_Pass",
                },
                new RegisterRequestBody
                {
                    Email = "d@a.com",
                    UserName = "ddd",
                    Password = "A1!_Pass",
                },
                new RegisterRequestBody
                {
                    Email = "e@a.com",
                    UserName = "eee",
                    Password = "A1!_Pass",
                },
                new RegisterRequestBody
                {
                    Email = "f@a.com",
                    UserName = "fff",
                    Password = "A1!_Pass",
                },
                new RegisterRequestBody
                {
                    Email = "g@a.com",
                    UserName = "ggg",
                    Password = "A1!_Pass",
                },
                new RegisterRequestBody
                {
                    Email = "h@a.com",
                    UserName = "hhh",
                    Password = "A1!_Pass",
                },
            };

            foreach (var user in mockUsers)
            {
                var registerResponse = await RegisterUser(user);

                if (registerResponse is BadRequestObjectResult)
                {
                    Console.WriteLine($"Unable to create user: {user.UserName}");
                    continue;
                }

                string userId = (string)((OkObjectResult)registerResponse).Value;

                var request = new RestRequest(@$"http://spotify_manager/api/spotify/portfolio/mock-generate?userId={userId}", Method.Get);

                var response = await spotifyManager.ExecuteAsync(request);

                if (!response.IsSuccessful)
                {
                    Console.WriteLine($"Unable to create spotify portfolio: {user.UserName}");
                }
            }

            return Ok();
        }

        private async Task<RestResponse> CreateProfile(string userId)
        {
            var request = new RestRequest(@"http://profiles/api/profiles/", Method.Post);
            request.AddHeader("Content-Type", "application/json");

            var body = "{\"" + "userId" + "\": \"" + $"{userId}" + "\"}";
            request.AddBody(body, "application/json");

            var response = await profileClient.ExecuteAsync(request);
            return response;
        }

        private async Task<RestResponse> CreateMatchProfile(string userId)
        {
            var request = new RestRequest(@"http://matchmaking/api/matchmaking/profile/create/", Method.Post);
            request.AddHeader("Content-Type", "application/json");

            var body = "{\"" + "userId" + "\": \"" + $"{userId}" + "\"}";
            request.AddBody(body, "application/json");

            var response = await matchmakingClient.ExecuteAsync(request);
            return response;
        }
    }
}
