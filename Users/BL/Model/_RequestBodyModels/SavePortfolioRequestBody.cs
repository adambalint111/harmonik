﻿using System.ComponentModel.DataAnnotations;

namespace Users.BL.Model
{
    public class SavePortfolioRequestBody
    {
        [Required]
        public string userId { get; set; }

        [Required]
        public string porftolioId { get; set; }
    }
}
