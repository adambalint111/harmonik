﻿using Microsoft.AspNetCore.Identity;

namespace Users.BL.Model
{
    public class ApplicationUser : IdentityUser
    {
        public string? SpotifyPortfolioId { get; set; }

        public ApplicationUser() : base()
        {

        }
    }
}
