﻿namespace Users.BL.Model
{
    public class AuthenticationResult
    {
        public string UserId { get; set; }

        public string Token { get; set; }

        public DateTime ExpiryDate { get; set; }
    }
}
