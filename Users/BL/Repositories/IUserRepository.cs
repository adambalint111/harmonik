﻿using Microsoft.AspNetCore.Identity;
using Users.BL.Model;

namespace Users.BL.Repositories
{
    public interface IUserRepository
    {
        public Task<string> RegisterUserAsync(RegisterRequestBody registerBody);

        public Task<AuthenticationResult> LoginUserAsync(LoginRequestBody loginBody);

        public bool ValidateToken(string token);
        
        public IEnumerable<ApplicationUser> GetAllUsersAsync();

        public Task<ApplicationUser> GetUserById(string id);
        
        public Task<int> SaveChangesAsync();
    }
}
