﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Users.BL.Model;
using Users.BL.Repositories;
using Users.DAL.Data;

namespace Users.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private UserManager<ApplicationUser> userManager;
        private ApplicationDbContext userContext;
        private IConfiguration configuration;
        private TokenValidationParameters tokenValidationParameters;

        public UserRepository(UserManager<ApplicationUser> userManager, ApplicationDbContext userContext, IConfiguration configuration, TokenValidationParameters tokenValidationParameters)
        {
            this.userManager = userManager;
            this.userContext = userContext;
            try
            {
                userContext.Database.EnsureCreated();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.configuration = configuration;
            this.tokenValidationParameters = tokenValidationParameters;
        }

        public IEnumerable<ApplicationUser> GetAllUsersAsync()
        {
            return userContext.Users.ToList();
        }

        public async Task<ApplicationUser> GetUserById(string id)
        {
            var user = await userManager.FindByIdAsync(id);

            return user;
        }

        public async Task<string> RegisterUserAsync(RegisterRequestBody registerBody)
        {
            ApplicationUser user = new ApplicationUser()
            {
                UserName = registerBody.UserName,
                Email = registerBody.Email,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var result = await userManager.CreateAsync(user, registerBody.Password);
            
            return result.Succeeded ? user.Id : "";
        }

        public async Task<AuthenticationResult> LoginUserAsync(LoginRequestBody loginBody)
        {
            var user = await userManager.FindByEmailAsync(loginBody.Email);
            
            if (user == null || !(await userManager.CheckPasswordAsync(user, loginBody.Password)))
            {
                return null;
            }

            return await GenerateJWTTokenAsync(user, null);
        }

        public bool ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = configuration["JWT:SecretKey"];

            try
            {
                tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                return jwtToken != null;
            }
            catch
            {
                return false;
            }
        }

        public async Task<int> SaveChangesAsync()
        {
            return await userContext.SaveChangesAsync();
        }

        private async Task<AuthenticationResult> GenerateJWTTokenAsync(ApplicationUser user, string? refreshToken)
        {
            var authClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["JWT:SecretKey"]));
            var token = new JwtSecurityToken(
                    issuer: configuration["JWT:Issuer"],
                    audience: configuration["JWT:Audience"],
                    expires: DateTime.UtcNow.AddMinutes(10),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            var response = new AuthenticationResult()
            {
                UserId = user.Id,
                Token = jwtToken,
                ExpiryDate = token.ValidTo
            };

            return response;
        }
    }
}
