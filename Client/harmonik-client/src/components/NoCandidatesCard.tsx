import { Box } from "@chakra-ui/react";

export const NoCandidatesCard = (props: { text: string }) => {
  return (
    <Box
      backgroundColor="blue.100"
      p={5}
      borderRadius="md"
      boxShadow="md"
      color="blue.800"
    >
      {props.text}
    </Box>
  );
};
