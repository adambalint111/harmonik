import {
  Box,
  Button,
  Table,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Tr,
} from "@chakra-ui/react";
import React from "react";
import { PortfolioData } from "../interfaces/SpotifyData";

const PortfolioTable = (props: { data: PortfolioData | undefined }) => {
  const mappedData = props.data?.favouriteTracks.map((element) => (
    <Tr>
      <Td>{element.name}</Td>
      <Td>
        <Button colorScheme="teal">
          <a
            href={`https://open.spotify.com/track/${element.id}`}
            target="_blank"
          >
            Check out
          </a>
        </Button>
      </Td>
    </Tr>
  ));

  return (
    <React.Fragment>
      <TableContainer>
        <Table variant="simple" size="sm">
          <TableCaption placement="top" fontSize="2xl">
            Favourite Tracks
          </TableCaption>
          <Tbody>
            {mappedData}
            <Tr>
              <Td>Prefered Acousticness:</Td>{" "}
              <Td>{props.data?.preferedAcousticness}</Td>
            </Tr>
            <Tr>
              <Td>Prefered Danceability:</Td>{" "}
              <Td>{props.data?.preferedDanceability}</Td>
            </Tr>
            <Tr>
              <Td>Prefered Energy:</Td> <Td>{props.data?.preferedEnergy}</Td>
            </Tr>
            <Tr>
              <Td>Prefered Valence:</Td> <Td>{props.data?.preferedValence}</Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
};

export default PortfolioTable;
