import {
  Button,
  Container,
  FormControl,
  FormLabel,
  Input,
  Select,
  Textarea,
} from "@chakra-ui/react";
import { SubmitHandler, useForm } from "react-hook-form";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { UnauthorizedError } from "../errors/UnauthorizedError";
import { ProfileData } from "../interfaces/ProfileData";
import { SubmitEditProfile } from "../services/ProfileService";

const EditProfile = () => {
  const navigate = useNavigate();

  const { register, handleSubmit } = useForm<ProfileData>();

  const location = useLocation();
  const profileData: ProfileData = location.state as ProfileData;

  const onSubmit: SubmitHandler<ProfileData> = async (data) => {
    const body: any = {
      ...data,
      profileId: profileData.profileId,
      userId: profileData.userId,
    };

    body.sex = body?.sex === "1" ? true : false;
    body.age = parseInt(body?.age);

    SubmitEditProfile(body)
      .then(() => navigate("/profile"))
      .catch((e: UnauthorizedError) => navigate("/login"));
  };

  return (
    <Container minW="container.md" mt="12">
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl mt="4">
          <FormLabel htmlFor="displayName">Name</FormLabel>
          <Input
            id="displayName"
            placeholder="Name"
            defaultValue={profileData?.displayName}
            {...register("displayName")}
          />
        </FormControl>
        <FormControl mt="4">
          <FormLabel htmlFor="age">Age</FormLabel>
          <Input
            type="number"
            id="age"
            placeholder="Age"
            defaultValue={profileData?.age}
            {...register("age")}
          />
        </FormControl>
        <FormControl mt="4">
          <FormLabel htmlFor="sex">Sex</FormLabel>
          <Select
            id="sex"
            defaultValue={profileData?.sex ? 1 : 0}
            {...register("sex")}
          >
            <option value={1}>Male</option>
            <option value={0}>Female</option>
          </Select>
        </FormControl>
        <FormControl mt="4">
          <FormLabel htmlFor="biography">Bio</FormLabel>
          <Textarea
            id="biography"
            placeholder="Biography"
            defaultValue={profileData?.biography}
            {...register("biography")}
          />
        </FormControl>
        <FormControl mt="4">
          <FormLabel htmlFor="pictureUrl">Profile picture</FormLabel>
          <Input
            id="pictureUrl"
            placeholder="Paste URL of image"
            defaultValue={profileData?.pictureUrl}
            {...register("pictureUrl")}
          />
        </FormControl>
        <Button m={4} colorScheme="teal" type="submit">
          Submit
        </Button>
        <Link to="/profile">
          <Button m={4} colorScheme="gray">
            Cancel
          </Button>
        </Link>
      </form>
    </Container>
  );
};

export default EditProfile;
