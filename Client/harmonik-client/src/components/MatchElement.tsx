import { Avatar, Box, Button, Flex, Text, WrapItem } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { Match } from "../interfaces/MatchData";
import { ProfileData } from "../interfaces/ProfileData";

export const MatchElement = (props: {
  element: Match;
  profile: ProfileData | undefined;
}) => {
  return (
    <Flex
      justifyContent="center"
      bg="blue.100"
      py={5}
      my={5}
      boxShadow="md"
      borderRadius="md"
    >
      <WrapItem w="25%" ml={5}>
        <Avatar
          size="2xl"
          name={props.profile?.displayName}
          src={props.profile?.pictureUrl}
        />
      </WrapItem>
      <Box w="75%" textAlign="left">
        <Box>
          <Text
            textColor="pink.800"
            fontWeight="bold"
            mr={2}
            display="inline-block"
          >
            Name:
          </Text>
          {props.profile?.displayName}
        </Box>
        <Box>
          <Text
            textColor="pink.800"
            fontWeight="bold"
            mr={2}
            display="inline-block"
          >
            Rate:
          </Text>
          {props.element.matchRate * 100}%
        </Box>
        <Box>
          <Text
            textColor="pink.800"
            fontWeight="bold"
            mr={2}
            display="inline-block"
          >
            Shared Favorites:
          </Text>
          {props.element.sharedFavourites}
        </Box>
        <Flex mt={10} mr={5} justifyContent="right">
          <Link to={`/userprofile/${props.element?.userId}`}>
            <Button backgroundColor="cyan.400" color="white">
              Open Profile
            </Button>
          </Link>
        </Flex>
      </Box>
    </Flex>
  );
};
