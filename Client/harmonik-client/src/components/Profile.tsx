import {
  Avatar,
  Box,
  Button,
  Center,
  Container,
  Divider,
  Flex,
  Text,
  WrapItem,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UnauthorizedError } from "../errors/UnauthorizedError";
import { ProfileData } from "../interfaces/ProfileData";
import { PortfolioData } from "../interfaces/SpotifyData";
import { GetProfile } from "../services/ProfileService";
import { GetPortfolio, LoginSpotify } from "../services/SpotifyService";
import PortfolioTable from "./PortfolioTable";

const Profile = () => {
  const navigate = useNavigate();
  const [profileData, setProfileData] = useState<ProfileData>();
  const [portfolioData, setPortfolioData] = useState<PortfolioData>();
  const [isConnectedToSpotify, setConnectedToSpotify] = useState<boolean>();
  useEffect(() => {
    GetProfile("")
      .then(setProfileData)
      .catch((e: UnauthorizedError) => navigate("/login"));
    GetPortfolioFromService();
  }, []);

  const GetPortfolioFromService = async () => {
    const portfolio = await GetPortfolio("").catch((e: UnauthorizedError) =>
      navigate("/login")
    );

    if (portfolio) {
      setConnectedToSpotify(true);
      setPortfolioData(portfolio);
    } else {
      setConnectedToSpotify(false);
      setPortfolioData(undefined);
    }
  };

  const portfolioStack = (
    <React.Fragment>
      <PortfolioTable data={portfolioData}></PortfolioTable>
    </React.Fragment>
  );

  const spotifyButton = (
    <Button backgroundColor="pink.800" color="white" onClick={LoginSpotify}>
      Connect to Spotify
    </Button>
  );

  return (
    <Flex mt={12} px={12} justifyContent="center">
      <Box w="70%">
        <Flex
          backgroundColor="blue.100"
          p={5}
          borderRadius="md"
          boxShadow="md"
          color="blue.800"
        >
          <WrapItem w="25%">
            <Avatar
              size="2xl"
              name={profileData?.displayName}
              src={profileData?.pictureUrl}
            />
          </WrapItem>
          <Box pl={5} mt={12} minW="75%">
            <Box h="30px" textAlign="left">
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Name:
              </Text>
              {profileData?.displayName || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
            <Box h="30px" textAlign="left">
              {" "}
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Age:
              </Text>
              {profileData?.age || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
            <Box h="30px" textAlign="left">
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Sex:
              </Text>
              {profileData?.sex ? "Male" : "Female" || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
            <Box h="30px" textAlign="left">
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Bio:
              </Text>
              {profileData?.biography || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
            <Flex mt={10} justifyContent="right">
              <Link to="/editprofile" state={profileData}>
                <Button backgroundColor="cyan.400" color="white">
                  Edit Profile
                </Button>
              </Link>
            </Flex>
          </Box>
        </Flex>
      </Box>
      <Center height="300px" w="5%" ml="8">
        <Divider orientation="vertical" />
      </Center>
      <Container w="25%">
        {isConnectedToSpotify ? portfolioStack : spotifyButton}
      </Container>
    </Flex>
  );
};

export default Profile;
