import {
  Button,
  Container,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Text,
} from "@chakra-ui/react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate, Link } from "react-router-dom";
import React from "react";
import { SubmitRegister } from "../services/UserService";
import { BadRequestError } from "../errors/BadRequestError";

export interface RegisterInputs {
  username: string;
  email: string;
  password: string;
}

const Register = () => {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<RegisterInputs>();

  const onSubmit: SubmitHandler<RegisterInputs> = async (data) => {
    SubmitRegister(data)
      .then(() => navigate("/login"))
      .catch((e: BadRequestError) =>
        alert(
          "Error during registration!\n\nEither there is a user already created with the provided email address, or the password is not appropriate. Please provide a password with at least 6 characters, including numbers, lowercase letters, uppercase letters, and symbols."
        )
      );
  };
  return (
    <Container mt="6rem">
      <Text fontSize="2xl">Register to Harmonik</Text>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl isInvalid={errors.username !== undefined}>
          <FormLabel htmlFor="username">Username</FormLabel>
          <Input
            id="username"
            placeholder="username"
            {...register("username", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>
            {errors.username && errors.username.message}
          </FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.email !== undefined}>
          <FormLabel htmlFor="email">Email</FormLabel>
          <Input
            type="email"
            id="email"
            placeholder="email"
            {...register("email", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>
            {errors.email && errors.email.message}
          </FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.password !== undefined}>
          <FormLabel htmlFor="password">Password</FormLabel>
          <Input
            type="password"
            id="password"
            placeholder="password"
            {...register("password", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>
            {errors.password && errors.password.message}
          </FormErrorMessage>
        </FormControl>
        <Button
          mt={4}
          colorScheme="teal"
          isLoading={isSubmitting}
          type="submit"
        >
          Register
        </Button>
      </form>
      <Link to="/login">
        <Button mt={4} colorScheme="teal" variant="outline">
          Login
        </Button>
      </Link>
    </Container>
  );
};

export default Register;
