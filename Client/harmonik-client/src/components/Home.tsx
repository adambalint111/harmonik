import React, { useEffect, useState } from "react";
import { Box, Button, Center, Image, Text, VStack } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { GetAuth } from "../services/UserService";

const Home = () => {
  const [isAuthenticated, setAuthenticated] = useState(true);
  const [home, setHome] = useState<JSX.Element>(<React.Fragment />);
  useEffect(() => {
    GetAuth().then(setAuthenticated);
  }, []);

  useEffect(() => {
    isAuthenticated
      ? setHome(
          <Box
            p={12}
            w="100%"
            backgroundColor="blue.100"
            boxShadow="md"
            color="blue.800"
          >
            <Text textColor="pink.800" fontSize="xl">
              Welcome to Harmonik
            </Text>
            <Text textColor="pink.800" fontSize="md" fontStyle="italic">
              We wish you will find your match here
            </Text>
          </Box>
        )
      : setHome(
          <VStack>
            <Image
              mt="2rem"
              borderRadius="10px"
              boxSize="10rem"
              src="https://img.freepik.com/free-vector/harp-flat-icon-illustration-isolated-vector-sign-symbol_159242-2945.jpg"
            />
            <Box fontStyle="italic">
              Harmonik is a dating site for people that love music
            </Box>
            <Link to="/register">
              <Button mt={4} colorScheme="teal">
                Sign-up
              </Button>
            </Link>
            <Link to="/login">
              <Button mt={1} colorScheme="teal" variant="outline">
                Sign-in
              </Button>
            </Link>
          </VStack>
        );
  }, [isAuthenticated]);
  return <Center>{home}</Center>;
};

export default Home;
