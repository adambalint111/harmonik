import React, { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { SaveToken } from "../services/SpotifyService";

const SpotifyCallback = () => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  useEffect(() => {
    SaveToken(searchParams.get("code")).then(() => {
      navigate("/profile");
    });
  }, []);

  return <div>Loading...</div>;
};

export default SpotifyCallback;
