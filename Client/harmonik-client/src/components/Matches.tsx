import {
  Box,
  Button,
  Center,
  Container,
  Flex,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UnauthorizedError } from "../errors/UnauthorizedError";
import { Match, MatchData } from "../interfaces/MatchData";
import { ProfileData } from "../interfaces/ProfileData";
import {
  DecideCandidate,
  GenerateMatches,
  GetMatchProfile,
} from "../services/MatchmakingService";
import { GetProfiles } from "../services/ProfileService";
import { MatchElement } from "./MatchElement";
import { NoCandidatesCard } from "./NoCandidatesCard";

const Matches = () => {
  const navigate = useNavigate();
  const [matchData, setMatchData] = useState<MatchData>();
  const [profiles, setProfiles] = useState<ProfileData[]>();
  useEffect(() => {
    GetMatchProfile()
      .then(setMatchData)
      .catch((e: UnauthorizedError) => navigate("/login"));
  }, []);
  useEffect(() => {
    GetProfiles(matchData)
      .then(setProfiles)
      .catch((e: UnauthorizedError) => navigate("/login"));
  }, [matchData]);

  const refreshDataCallback = async () => {
    const matchData = await GetMatchProfile().catch((e: UnauthorizedError) =>
      navigate("/login")
    );
    if (matchData) setMatchData(matchData);
  };

  const mappedCandidates = matchData?.candidates?.map((e) => (
    <Box
      borderRadius="md"
      border="1px solid"
      borderColor="gray.400"
      px={5}
      my={5}
    >
      <MatchElement
        element={e}
        profile={profiles?.find((x) => x.userId === e.userId)}
      />
      <Flex mb={5} justifyContent="right">
        <Button
          mr={3}
          backgroundColor="pink.800"
          color="white"
          onClick={() =>
            DecideCandidate(e.userId, true, refreshDataCallback).catch(
              (e: UnauthorizedError) => navigate("/login")
            )
          }
        >
          Approve
        </Button>
        <Button
          onClick={() =>
            DecideCandidate(e.userId, false, refreshDataCallback).catch(
              (e: UnauthorizedError) => navigate("/login")
            )
          }
        >
          Reject
        </Button>
      </Flex>
    </Box>
  ));

  const mappedAccCandidates = matchData?.approvedCandidates?.map((e) => (
    <MatchElement
      element={e}
      profile={profiles?.find((x) => x.userId === e.userId)}
    />
  ));

  const mappedRejCandidates = matchData?.rejectedCandidates?.map((e) => (
    <MatchElement
      element={e}
      profile={profiles?.find((x) => x.userId === e.userId)}
    />
  ));

  const mappedMatches = matchData?.matches?.map((e) => (
    <MatchElement
      element={e}
      profile={profiles?.find((x) => x.userId === e.userId)}
    />
  ));

  return (
    <Center mt="12">
      <Box w="70%">
        <Tabs size="lg" isFitted variant="enclosed">
          <TabList mb="1em">
            <Tab>Candidates</Tab>
            <Tab>Accepted Candidates</Tab>
            <Tab>Rejected Candidates</Tab>
            <Tab>Matches</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              {mappedCandidates && mappedCandidates.length !== 0 ? (
                mappedCandidates
              ) : (
                <React.Fragment>
                  <NoCandidatesCard text="There are no candidates to show at the moment. Try generating some more!" />
                  <Container mt={5}>
                    <Button
                      onClick={() => GenerateMatches(refreshDataCallback)}
                      ml="8"
                    >
                      Generate Candidates
                    </Button>
                  </Container>
                </React.Fragment>
              )}
            </TabPanel>
            <TabPanel>
              {mappedAccCandidates && mappedAccCandidates.length !== 0 ? (
                mappedAccCandidates
              ) : (
                <NoCandidatesCard text="There are no accepted candidates to show at the moment." />
              )}
            </TabPanel>
            <TabPanel>
              {mappedRejCandidates && mappedRejCandidates.length !== 0 ? (
                mappedRejCandidates
              ) : (
                <NoCandidatesCard text="There are no rejected candidates to show at the moment." />
              )}
            </TabPanel>
            <TabPanel>
              {mappedMatches && mappedMatches.length !== 0 ? (
                mappedMatches
              ) : (
                <NoCandidatesCard text="There are no matches to show at the moment." />
              )}
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </Center>
  );
};

export default Matches;
