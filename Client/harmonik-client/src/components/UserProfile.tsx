import {
  Avatar,
  Box,
  Text,
  Flex,
  WrapItem,
  Divider,
  Center,
  Container,
} from "@chakra-ui/react";
import React from "react";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { UnauthorizedError } from "../errors/UnauthorizedError";
import { ProfileData } from "../interfaces/ProfileData";
import { PortfolioData } from "../interfaces/SpotifyData";
import { GetProfile } from "../services/ProfileService";
import { GetPortfolio } from "../services/SpotifyService";
import PortfolioTable from "./PortfolioTable";

const UserProfile = () => {
  const navigate = useNavigate();
  const { userId } = useParams();
  const [userData, setUserData] = useState<ProfileData>();
  const [portfolioData, setPortfolioData] = useState<PortfolioData>();
  useEffect(() => {
    GetProfile(userId || "")
      .then(setUserData)
      .catch((e: UnauthorizedError) => navigate("/login"));
    GetPortfolio(userId || "")
      .then(setPortfolioData)
      .catch((e: UnauthorizedError) => navigate("/login"));
  }, []);

  const portfolioStack = (
    <React.Fragment>
      <PortfolioTable data={portfolioData}></PortfolioTable>
    </React.Fragment>
  );

  return (
    <Flex mt={12} px={12} justifyContent="center">
      <Box w="70%">
        <Flex
          backgroundColor="blue.100"
          p={5}
          borderRadius="md"
          boxShadow="md"
          color="blue.800"
        >
          <WrapItem w="25%">
            <Avatar
              size="2xl"
              name={userData?.displayName}
              src={userData?.pictureUrl}
            />
          </WrapItem>
          <Box pl={5} mt={12} minW="75%">
            <Box h="30px" textAlign="left">
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Name:
              </Text>
              {userData?.displayName || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
            <Box h="30px" textAlign="left">
              {" "}
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Age:
              </Text>
              {userData?.age || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
            <Box h="30px" textAlign="left">
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Sex:
              </Text>
              {userData?.sex ? "Male" : "Female" || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
            <Box h="30px" textAlign="left">
              <Text
                textColor="pink.800"
                fontWeight="bold"
                mr={2}
                display="inline-block"
              >
                Bio:
              </Text>
              {userData?.biography || "empty"}
            </Box>
            <Divider borderColor="pink.800" my={2} />
          </Box>
        </Flex>
      </Box>
      <Center height="300px" w="5%" ml="8">
        <Divider orientation="vertical" />
      </Center>
      <Container w="25%">{portfolioStack}</Container>
    </Flex>
  );
};
export default UserProfile;
