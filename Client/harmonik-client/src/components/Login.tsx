import {
  Button,
  Container,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Text,
} from "@chakra-ui/react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate, Link } from "react-router-dom";
import { UnauthorizedError } from "../errors/UnauthorizedError";
import { SubmitLogin } from "../services/UserService";

export interface LoginInputs {
  email: string;
  password: string;
}

const Login = () => {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<LoginInputs>();

  const onSubmit: SubmitHandler<LoginInputs> = async (data) => {
    SubmitLogin(data)
      .then(() => navigate("/profile"))
      .catch((e: UnauthorizedError) =>
        alert(
          "Error during login!\n\nEither the email address or the password is wrong."
        )
      );
  };

  return (
    <Container mt="6rem">
      <Text fontSize="2xl">Login to Harmonik</Text>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl isInvalid={errors.email !== undefined}>
          <FormLabel htmlFor="email">Email</FormLabel>
          <Input
            type="email"
            id="email"
            placeholder="email"
            {...register("email", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>
            {errors.email && errors.email.message}
          </FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.password !== undefined}>
          <FormLabel htmlFor="password">Password</FormLabel>
          <Input
            type="password"
            id="password"
            placeholder="password"
            {...register("password", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>
            {errors.password && errors.password.message}
          </FormErrorMessage>
        </FormControl>
        <Button
          mt={4}
          colorScheme="teal"
          isLoading={isSubmitting}
          type="submit"
        >
          Login
        </Button>
      </form>
      <Link to="/register">
        <Button mt={4} colorScheme="teal" variant="outline">
          Register
        </Button>
      </Link>
    </Container>
  );
};

export default Login;
