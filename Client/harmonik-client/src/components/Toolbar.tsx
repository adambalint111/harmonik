import {
  Box,
  Center,
  Flex,
  Icon,
  Image,
  Spacer,
  Square,
} from "@chakra-ui/react";
import { FiLogOut } from "react-icons/fi";
import React from "react";
import { Link, useNavigate } from "react-router-dom";

const Toolbar = () => {
  const navigate = useNavigate();
  return (
    <Box zIndex={100} sx={{ position: "sticky", top: "0" }}>
      <Flex bg="blue.100" boxShadow="md" height="60px">
        <Center ml="3">
          <Image
            borderRadius="10px"
            boxSize="40px"
            src="https://img.freepik.com/free-vector/harp-flat-icon-illustration-isolated-vector-sign-symbol_159242-2945.jpg"
          />
        </Center>
        <Center ml="3" mr="3">
          <Box ml="3" mr="3" textColor="pink.800" fontWeight="bold">
            <Link to="/">Home</Link>
          </Box>
          <Box ml="3" mr="3" textColor="pink.800" fontWeight="bold">
            <Link to="/matches">Matches</Link>
          </Box>
          <Box ml="3" mr="3" textColor="pink.800" fontWeight="bold">
            <Link to="/profile">Profile</Link>
          </Box>
        </Center>

        <Spacer />
        <Square
          onClick={() => {
            localStorage.clear();
            navigate("/login");
          }}
          mr="3"
          _hover={{ cursor: "pointer" }}
        >
          <Icon w={8} h={8} as={FiLogOut} color="pink.800" />
        </Square>
      </Flex>
    </Box>
  );
};

export default Toolbar;
