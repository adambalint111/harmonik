import React, { useEffect, useState } from "react";
import "./App.css";
import {
  BrowserRouter,
  Routes,
  Route,
  Outlet,
  Navigate,
} from "react-router-dom";
import Profile from "./components/Profile";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import EditProfile from "./components/EditProfile";
import Toolbar from "./components/Toolbar";
import SpotifyCallback from "./components/SpotifyCallback";
import Matches from "./components/Matches";
import UserProfile from "./components/UserProfile";
import { GetAuth } from "./services/UserService";

const Unauthorized = () => {
  const [isAuthenticated, setAuthenticated] = useState(true);
  const [result, setResult] = useState<JSX.Element>(<React.Fragment />);
  useEffect(() => {
    GetAuth().then(setAuthenticated);
  }, []);

  useEffect(() => {
    isAuthenticated
      ? setResult(<Outlet />)
      : setResult(<Navigate to="/login" />);
  }, [isAuthenticated]);

  return result;
};

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Toolbar></Toolbar>
        <Routes>
          <Route index element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/" element={<Unauthorized />}>
            <Route path="/profile" element={<Profile />} />
          </Route>
          <Route path="/" element={<Unauthorized />}>
            <Route path="/matches" element={<Matches />} />
          </Route>
          <Route path="/" element={<Unauthorized />}>
            <Route path="/editprofile" element={<EditProfile />} />
          </Route>
          <Route path="/" element={<Unauthorized />}>
            <Route path="/userprofile/:userId" element={<UserProfile />} />
          </Route>
          <Route path="/callback" element={<SpotifyCallback />} />
          <Route path="*" element={<div>404 Page Not Found</div>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
