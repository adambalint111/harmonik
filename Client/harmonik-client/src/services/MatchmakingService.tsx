import { UnauthorizedError } from "../errors/UnauthorizedError";
import { Match, MatchData } from "../interfaces/MatchData";

export const DecideCandidate = async (
  otherId: string,
  accepted: boolean,
  callback: Function
): Promise<boolean | undefined> => {
  const userId = localStorage.getItem("userId");

  const body: any = {
    otherId: otherId,
    accepted: accepted,
  };

  const requestOptions: RequestInit = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorize: "Bearer " + localStorage.getItem("token"),
    },
    body: JSON.stringify(body),
  };

  const response = await fetch(
    "http://localhost:5083/api/matchmaking/profile/" +
      userId +
      "/decide-candidate",
    requestOptions
  );
  if (response.status === 200) {
    const matched: boolean = await response.json();
    await callback();
    return matched;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};

export const GetMatchProfile = async (): Promise<MatchData | undefined> => {
  const userId = localStorage.getItem("userId");

  const response = await fetch(
    "http://localhost:5083/api/matchmaking/profile/" + userId,
    {
      method: "GET",
      headers: {
        Authorize: "Bearer " + localStorage.getItem("token"),
      },
    }
  );
  if (response.status === 200) {
    const data: MatchData = await response.json();
    return data;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};

export const GenerateMatches = async (
  callback: Function
): Promise<Match[] | undefined> => {
  const userId = localStorage.getItem("userId");

  const response = await fetch(
    "http://localhost:5083/api/matchmaking/possible-matches?userId=" + userId,
    {
      method: "GET",
      headers: {
        Authorize: "Bearer " + localStorage.getItem("token"),
      },
    }
  );
  if (response.status === 200) {
    const data: Match[] = await response.json();
    GetMatchProfile();
    await callback();
    return data;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};
