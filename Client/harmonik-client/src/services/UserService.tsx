import { LoginInputs } from "../components/Login";
import { RegisterInputs } from "../components/Register";
import { BadRequestError } from "../errors/BadRequestError";
import { UnauthorizedError } from "../errors/UnauthorizedError";

export const GetAuth = async (): Promise<boolean> => {
  let token = localStorage.getItem("token") || "";
  const response = await fetch("http://localhost:5083/api/auth/", {
    method: "GET",
    headers: {
      Authorize: "Bearer " + token,
    },
  });
  if (response.status === 200) {
    return true;
  }
  return false;
};

export const SubmitLogin = async (inputs: LoginInputs) => {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(inputs),
  };

  const response = await fetch(
    "http://localhost:5083/api/users/login",
    requestOptions
  );
  if (response.status === 200) {
    const data = await response.json();
    localStorage.setItem("token", data.token);
    localStorage.setItem("expiryDate", data.expiryDate);
    localStorage.setItem("userId", data.userId);

    return;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};

export const SubmitRegister = async (inputs: RegisterInputs) => {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(inputs),
  };

  const response = await fetch(
    "http://localhost:5083/api/users/register",
    requestOptions
  );
  if (response.status === 200) {
    const responseData = await response.text();
    localStorage.setItem("userId", responseData);
  }
  if (response.status === 400) {
    throw new BadRequestError("unauthorized");
  }
};
