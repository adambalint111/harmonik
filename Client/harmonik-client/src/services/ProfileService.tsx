import { UnauthorizedError } from "../errors/UnauthorizedError";
import { MatchData } from "../interfaces/MatchData";
import { ProfileData } from "../interfaces/ProfileData";

export const GetProfile = async (
  userId: string
): Promise<ProfileData | undefined> => {
  if (userId === "") {
    userId = localStorage.getItem("userId") || "";
  }

  const response = await fetch(
    "http://localhost:5083/api/profiles/userid/" + userId,
    {
      method: "GET",
      headers: {
        Authorize: "Bearer " + localStorage.getItem("token"),
      },
    }
  );
  if (response.status === 200) {
    const data: ProfileData = await response.json();
    return data;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};

export const SubmitEditProfile = async (body: any) => {
  const response = await fetch("http://localhost:5083/api/profiles", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorize: "Bearer " + localStorage.getItem("token"),
    },
    body: JSON.stringify(body),
  });
  if (response.status === 201) {
    return;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};

export const GetProfiles = async (
  matchData: MatchData | undefined
): Promise<ProfileData[] | undefined> => {
  let profiles = matchData?.approvedCandidates ?? [];
  const ids = profiles
    .concat(matchData?.candidates ?? [])
    .concat(matchData?.rejectedCandidates ?? [])
    .concat(matchData?.matches ?? [])
    .map((m) => m.userId)
    .join(",");

  const response = await fetch(
    "http://localhost:5083/api/profiles/userids/" + ids,
    {
      method: "GET",
      headers: {
        Authorize: "Bearer " + localStorage.getItem("token"),
      },
    }
  );
  if (response.status === 200) {
    const data: ProfileData[] = await response.json();
    return data;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};
