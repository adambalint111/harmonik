import { UnauthorizedError } from "../errors/UnauthorizedError";
import { PortfolioData } from "../interfaces/SpotifyData";

export const LoginSpotify = async () => {
  const userId = localStorage.getItem("userId");

  const requestOptions: RequestInit = {
    method: "GET",
    headers: {
      Authorize: "Bearer " + localStorage.getItem("token"),
    },
    redirect: "follow",
  };

  const response = await fetch(
    "http://localhost:5083/api/spotify/login?userId=" + userId,
    requestOptions
  );
  if (response.status === 200) {
    const data: any = await response.text();
    window.location.href = data;
    return null;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};

export const GetPortfolio = async (
  userId: string
): Promise<PortfolioData | undefined> => {
  if (userId === "") {
    userId = localStorage.getItem("userId") || "";
  }

  const response = await fetch(
    "http://localhost:5083/api/spotify/portfolio/generate?userId=" + userId,
    {
      method: "GET",
      headers: {
        Authorize: "Bearer " + localStorage.getItem("token"),
      },
    }
  );
  if (response.status === 200) {
    const data: PortfolioData = await response.json();
    return data;
  }
  if (response.status === 401) {
    throw new UnauthorizedError("unauthorized");
  }
};

export const SaveToken = async (code: string | null) => {
  const userId = localStorage.getItem("userId");

  try {
    const response = await fetch(
      "http://localhost:5083/api/spotify/callback?code=" +
        code +
        "&userId=" +
        userId,
      {
        method: "GET",
        headers: {
          Authorize: "Bearer " + localStorage.getItem("token"),
        },
      }
    );
    if (response.status === 200) {
      return;
    }
  } catch (error) {
    console.log(error);
  }
};
