export interface Track {
  duration: number;
  href: string;
  id: string;
  name: string;
  previewUrl: string;
  uri: string;
}

export interface PortfolioData {
  favouriteTracks: Track[];
  preferedAcousticness: number;
  preferedDanceability: number;
  preferedEnergy: number;
  preferedValence: number;
}

export interface SpotifyToken {
  access_token: string;
  token_type: string;
  scope: string;
  expires_in: number;
  expiryDate: Date;
  refresh_token: string;
}
