export interface ProfileData {
  profileId: string;
  userId: string;
  displayName: string;
  age: number;
  sex: boolean;
  biography: string;
  pictureUrl: string;
}
