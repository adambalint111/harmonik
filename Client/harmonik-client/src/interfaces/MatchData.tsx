export interface MatchData {
  candidates: Match[];
  approvedCandidates: Match[];
  rejectedCandidates: Match[];
  matches: Match[];
}

export interface Match {
  userId: string;
  matchRate: number;
  sharedFavourites: number;
}
