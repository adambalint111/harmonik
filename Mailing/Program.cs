﻿using Mailing;
using MassTransit;

var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
{
    cfg.Host("rabbitmq");
    cfg.ReceiveEndpoint("match-created-event", e =>
    {
        e.Consumer<MatchCreatedConsumer>();
    });
});

var stateTimer = new Timer(MailingService.SendMessages, null, 1000, 60000);

await busControl.StartAsync(new CancellationToken());

try
{
    Console.WriteLine("Press enter to exit");

    await Task.Run(() => Console.ReadLine());
}
finally
{
    await busControl.StopAsync();
    stateTimer.Dispose();
}