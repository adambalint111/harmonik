﻿using MassTransit;
using Newtonsoft.Json;
using RestSharp;
using SharedModels;

namespace Mailing
{
    internal class MatchCreatedConsumer : IConsumer<MatchCreated>
    {
        private RestClient usersClient;
        private RestClient profilesClient;

        public MatchCreatedConsumer()
        {
            this.usersClient = new RestClient();
            this.profilesClient = new RestClient();
        }

        public async Task Consume(ConsumeContext<MatchCreated> context)
        {
            var getEmailRequest = new RestRequest(@$"http://users/api/emails/{context.Message.UserA}", Method.Get);
            var toEmail = (await usersClient.ExecuteAsync(getEmailRequest)).Content;

            var typeDefinition = new { name = "", imageUrl = "" };
            var getProfileInfoRequest = new RestRequest(@$"http://profiles/api/profiles/basicinfo/{context.Message.UserA}", Method.Get);
            var response1 = await profilesClient.ExecuteAsync(getProfileInfoRequest);
            var profileInfo = JsonConvert.DeserializeAnonymousType(response1.Content, typeDefinition);

            var getPartnerInfoRequest = new RestRequest(@$"http://profiles/api/profiles/basicinfo/{context.Message.UserB}", Method.Get);
            var response2 = await profilesClient.ExecuteAsync(getPartnerInfoRequest);
            var partnerInfo = JsonConvert.DeserializeAnonymousType(response2.Content, typeDefinition);

            if (response1.IsSuccessful && response2.IsSuccessful)
            {
                var newMessage = new Message
                {
                    ToEmail = toEmail,
                    NameOfUser = profileInfo.name,
                    NameOfPartner = partnerInfo.name,
                    ImageOfPartner = partnerInfo.imageUrl,
                    MatchRate = context.Message.MatchRate,
                    MatchDate = context.Message.MatchDate
                };

                MailingService.MessagesToSend.Add(newMessage);
            }            
        }
    }
}
