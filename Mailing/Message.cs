﻿namespace Mailing
{
    internal class Message
    {
        public string ToEmail { get; set; }

        public float MatchRate { get; set; }

        public string NameOfUser { get; set; }

        public string NameOfPartner { get; set; }

        public string ImageOfPartner { get; set; }

        public DateTime MatchDate { get; set; }
    }
}
