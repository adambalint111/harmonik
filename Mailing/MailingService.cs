﻿using MailKit.Net.Smtp;
using MassTransit.Transports;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mailing
{
    internal class MailingService
    {
        public static List<Message> MessagesToSend { get; set; } = new List<Message>();

        public static void SendMessages(object stateInfo)
        {
            var messageGroups = MessagesToSend.GroupBy(m => m.ToEmail);

            foreach (var item in messageGroups)
            {
                SendMessage(item);
            }

            MessagesToSend = new List<Message>();
        }

        private static void SendMessage(IGrouping<string, Message> messages)
        {
            var toEmail = messages.Key;
            var toName = messages.First().NameOfUser;
            StringBuilder text = new StringBuilder($"<h1>Hello {toName}!</h1>\n" +
                       $"<b>You matched with {messages.Count()} people today.</b>\n" +
                       $"<h3>Your new matches are:</h3>\n" +
                       $"<ul>");

            foreach (var item in messages)
            {
                text.Append($"<li><img src='{item.ImageOfPartner}' style='width:150px;height:150px'/>  Name: {item.NameOfPartner}, Rate: {item.MatchRate}, Date: {item.MatchDate.ToShortDateString()}</li>\n");
            }

            text.Append("</ul>");

            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("Harmonik", "noreply@harmonik.com"));
            mailMessage.To.Add(new MailboxAddress(toName, toEmail));
            mailMessage.Subject = "Your daily summary";
            mailMessage.Body = new TextPart("html")
            {

                Text = text.ToString()
            };

            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Connect("smtp.ethereal.email", 587, MailKit.Security.SecureSocketOptions.StartTlsWhenAvailable);
                smtpClient.Authenticate("ciara62@ethereal.email", "g2e338vjwA1XrvMuJD");
                smtpClient.Send(mailMessage);
                smtpClient.Disconnect(true);
            }

            Console.WriteLine("An email was sent.");
        }
    }
}
