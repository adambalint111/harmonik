﻿using Matchmaking.Model;
using Microsoft.EntityFrameworkCore;

namespace Matchmaking.Data
{
    public class MatchmakingContext : DbContext
    {
        public DbSet<MatchProfile> MatchProfiles { get; set; }

        public MatchmakingContext(DbContextOptions<MatchmakingContext> options) : base(options)
        {

        }
    }
}
