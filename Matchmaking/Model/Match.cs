﻿using Microsoft.EntityFrameworkCore;

namespace Matchmaking.Model
{
    [Owned]
    public class Match
    {
        public string UserId { get; set; }

        public float MatchRate { get; set; }

        public int SharedFavourites { get; set; }
    }
}
