﻿using System.ComponentModel.DataAnnotations;

namespace Matchmaking.Model
{
    public class DecisionBody
    {
        [Required]
        public string otherId { get; set; }

        [Required]
        public bool accepted { get; set; }
    }
}
