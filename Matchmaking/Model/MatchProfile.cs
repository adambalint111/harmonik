﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Matchmaking.Model
{
    public class MatchProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string? Id { get; set; }

        [Required]
        public string UserId { get; set; }

        public List<Match>? Candidates { get; set; }

        public List<Match>? ApprovedCandidates { get; set; }

        public List<Match>? RejectedCandidates { get; set; }

        public List<Match>? Matches { get; set; }
    }
}
