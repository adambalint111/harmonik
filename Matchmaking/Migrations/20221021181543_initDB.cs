﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Matchmaking.Migrations
{
    public partial class initDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MatchProfiles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchProfiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MatchProfiles_ApprovedCandidates",
                columns: table => new
                {
                    MatchProfileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MatchRate = table.Column<float>(type: "real", nullable: false),
                    SharedFavourites = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchProfiles_ApprovedCandidates", x => new { x.MatchProfileId, x.Id });
                    table.ForeignKey(
                        name: "FK_MatchProfiles_ApprovedCandidates_MatchProfiles_MatchProfileId",
                        column: x => x.MatchProfileId,
                        principalTable: "MatchProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchProfiles_Candidates",
                columns: table => new
                {
                    MatchProfileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MatchRate = table.Column<float>(type: "real", nullable: false),
                    SharedFavourites = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchProfiles_Candidates", x => new { x.MatchProfileId, x.Id });
                    table.ForeignKey(
                        name: "FK_MatchProfiles_Candidates_MatchProfiles_MatchProfileId",
                        column: x => x.MatchProfileId,
                        principalTable: "MatchProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchProfiles_Matches",
                columns: table => new
                {
                    MatchProfileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MatchRate = table.Column<float>(type: "real", nullable: false),
                    SharedFavourites = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchProfiles_Matches", x => new { x.MatchProfileId, x.Id });
                    table.ForeignKey(
                        name: "FK_MatchProfiles_Matches_MatchProfiles_MatchProfileId",
                        column: x => x.MatchProfileId,
                        principalTable: "MatchProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchProfiles_RejectedCandidates",
                columns: table => new
                {
                    MatchProfileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MatchRate = table.Column<float>(type: "real", nullable: false),
                    SharedFavourites = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchProfiles_RejectedCandidates", x => new { x.MatchProfileId, x.Id });
                    table.ForeignKey(
                        name: "FK_MatchProfiles_RejectedCandidates_MatchProfiles_MatchProfileId",
                        column: x => x.MatchProfileId,
                        principalTable: "MatchProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MatchProfiles_ApprovedCandidates");

            migrationBuilder.DropTable(
                name: "MatchProfiles_Candidates");

            migrationBuilder.DropTable(
                name: "MatchProfiles_Matches");

            migrationBuilder.DropTable(
                name: "MatchProfiles_RejectedCandidates");

            migrationBuilder.DropTable(
                name: "MatchProfiles");
        }
    }
}
