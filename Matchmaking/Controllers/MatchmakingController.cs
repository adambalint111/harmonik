using MassTransit;
using Matchmaking.Data;
using Matchmaking.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RestSharp;
using SharedModels;

namespace Matchmaking.Controllers
{
    [ApiController]
    [Route("/api/matchmaking")]
    public class MatchmakingController : ControllerBase
    {
        private MatchmakingContext context;

        private RestClient spotifyManager;

        private RestClient usersClient;

        private IPublishEndpoint publishEndpoint;

        public MatchmakingController(MatchmakingContext context, IPublishEndpoint publishEndpoint)
        {
            this.context = context;

            try
            {
                context.Database.EnsureCreated();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            spotifyManager = new RestClient();
            usersClient = new RestClient();

            this.publishEndpoint = publishEndpoint;
        }

        [HttpGet]
        public async Task<IActionResult> GetMatchProfiles()
        {
            return Ok(await context.MatchProfiles.ToListAsync());
        }

        [HttpGet("profile/{userId}")]
        public async Task<IActionResult> GetMatchProfileById(string userId)
        {
            var profile = context.MatchProfiles.FirstOrDefault(p => p.UserId == userId);
            if (profile == null)
            {
                return NotFound("Match Profile not found");
            }

            return Ok(profile);
        }

        [HttpGet("possible-matches")]
        public async Task<IActionResult> GetPossibleMatches([FromQuery] string userId)
        {
            var candidates = await GenerateCandidates(context.MatchProfiles.FirstOrDefault(mp => mp.UserId == userId));

            if (candidates != null)
            {
                return Ok(candidates);
            }

            return NotFound();
        }

        [HttpPost("profile/{userId}/decide-candidate")]
        public async Task<IActionResult> DecideOnCandidate(string userId, [FromBody] DecisionBody decision)
        {
            var matchProfile = context.MatchProfiles.FirstOrDefault(mp => mp.UserId == userId);
            var candidateProfile = context.MatchProfiles.FirstOrDefault(mp => mp.UserId == decision.otherId);
            var candidateMatch = matchProfile.Candidates.FirstOrDefault(m => m.UserId == decision.otherId);

            if (matchProfile == null || candidateProfile == null || candidateMatch == null)
            {
                return NotFound();
            }

            matchProfile.Candidates.Remove(candidateMatch);

            if (decision.accepted)
            {
                var match = candidateProfile.ApprovedCandidates.FirstOrDefault(m => m.UserId == userId);
                if (match != null)
                {
                    candidateProfile.ApprovedCandidates.Remove(match);
                    candidateProfile.Matches.Add(match);
                    matchProfile.Matches.Add(candidateMatch);
                    await context.SaveChangesAsync();
                    SendNotification(matchProfile.UserId, candidateProfile.UserId, match.MatchRate);
                    return Ok(true);
                }
                
                matchProfile.ApprovedCandidates.Add(candidateMatch);
            }
            else
            {
                matchProfile.RejectedCandidates.Add(candidateMatch);
            }

            await context.SaveChangesAsync();
            return Ok(false);
        }

        [HttpPost("profile/create")]
        public async Task<IActionResult> CreateNewMatchProfile([FromBody] MatchProfile profile)
        {
            await context.MatchProfiles.AddAsync(profile);
            await context.SaveChangesAsync();

            return CreatedAtAction("GetMatchProfileById", new { userId = profile.UserId }, profile);
        }


        private async void SendNotification(string userA, string userB, float matchRate)
        {
            await publishEndpoint.Publish<MatchCreated>(new
            {
                UserA = userA,
                UserB = userB,
                MatchRate = matchRate,
                MatchDate = DateTime.Now
            });

            await publishEndpoint.Publish<MatchCreated>(new
            {
                UserA = userB,
                UserB = userA,
                MatchRate = matchRate,
                MatchDate = DateTime.Now
            });
        }

        private async Task<IEnumerable<Match>> GenerateCandidates(MatchProfile matchProfile)
        {
            if (matchProfile == null)
            {
                return null;
            }

            var skipIds = string.Join(',', matchProfile.ApprovedCandidates.Concat(matchProfile.RejectedCandidates).Concat(matchProfile.Candidates).Concat(matchProfile.Matches).Select(mp => mp.UserId).Distinct());

            var request = new RestRequest(@$"http://spotify_manager/api/spotify/similar-portfolios?userId={matchProfile.UserId}&skipIds={skipIds}", Method.Get);

            var response = await spotifyManager.ExecuteAsync(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return null;
            }

            var candidates = JsonConvert.DeserializeObject<List<Match>>(response.Content);

            if (candidates != null)
            {
                matchProfile.Candidates.AddRange(candidates);
                await context.SaveChangesAsync();
            }

            return candidates;
        }
    }
}