using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Profiles.Data;
using Profiles.Model;
using System.ComponentModel.DataAnnotations;

namespace Profiles.Controllers
{
    [ApiController]
    [Route("/api/profiles")]
    public class ProfileController : ControllerBase
    {
        private ProfileContext profileContext;

        public ProfileController(ProfileContext context)
        {
            profileContext = context;

            try
            {
                context.Database.EnsureCreated();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Profile>>> GetProfiles()
        {
            return Ok(await profileContext.Profiles.ToListAsync());
        }

        [HttpGet("id/{id}")]
        public async Task<ActionResult<Profile>> GetProfileById(string id)
        {
            var profile = await profileContext.Profiles.FindAsync(id);
            if (profile == null)
            {
                return NotFound("Profile not found");
            }

            return Ok(profile);
        }

        [HttpGet("userid/{id}")]
        public async Task<ActionResult<Profile>> GetProfileByUserId(string id)
        {
            var profile = await profileContext.Profiles.FirstOrDefaultAsync(x => x.UserId == id);
            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

        [HttpGet("basicinfo/{id}")]
        public async Task<IActionResult> GetProfileInfoByUserId(string id)
        {
            var profile = await profileContext.Profiles.FirstOrDefaultAsync(x => x.UserId == id);
            if (profile == null)
            {
                return NotFound();
            }

            return Ok(new { Name = profile.DisplayName, ImageUrl = profile.PictureUrl });
        }

        [HttpGet("userids/{ids}")]
        public IActionResult GetProfilesByUserIds(string ids)
        {
            var idList = ids.Split(',');
            var profiles = profileContext.Profiles.Where(p => idList.Contains(p.UserId));
            if (profiles == null)
            {
                return BadRequest();
            }

            return Ok(profiles);
        }

        [HttpPost]
        public async Task<ActionResult<Profile>> PostProfile([FromBody] Profile profileData)
        {
            var profile = await profileContext.Profiles.FindAsync(profileData.ProfileId);
            if (profile == null)
            {
                await profileContext.Profiles.AddAsync(profileData);
                await profileContext.SaveChangesAsync();
                return CreatedAtAction("GetProfileById", new { id = profileData.ProfileId }, profileData);
            }
            else
            {
                profile.Age = profileData.Age;
                profile.DisplayName = profileData.DisplayName;
                profile.Biography = profileData.Biography;
                profile.Sex = profileData.Sex;
                profile.PictureUrl = profileData.PictureUrl;
                await profileContext.SaveChangesAsync();

                return CreatedAtAction("GetProfileById", new { id = profile.ProfileId }, profile);
            }
        }
    }
}