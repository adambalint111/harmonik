﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Profiles.Model
{
    public class Profile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string? ProfileId { get; set; }

        [Required]
        public string UserId { get; set; }

        public string? DisplayName { get; set; }

        public int? Age { get; set; }

        public bool? Sex { get; set; }

        public string? Biography { get; set; }

        public string? PictureUrl { get; set; }

        // TODO: add Hobbies
        //public List<string> Hobbies { get; set; }
    }
}
