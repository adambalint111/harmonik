﻿using Microsoft.EntityFrameworkCore;
using Profiles.Model;

namespace Profiles.Data
{
    public class ProfileContext : DbContext
    {
        public DbSet<Profile> Profiles { get; set; }

        public ProfileContext(DbContextOptions<ProfileContext> options) : base(options)
        {

        }
    }
}
