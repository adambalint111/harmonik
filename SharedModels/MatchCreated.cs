﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedModels
{
    public interface MatchCreated
    {
        public string UserA { get; set; }

        public string UserB { get; set; }

        public float MatchRate { get; set; }

        public DateTime MatchDate { get; set; }
    }
}
