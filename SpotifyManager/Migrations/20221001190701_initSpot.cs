﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpotifyManager.Migrations
{
    public partial class initSpot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SpotifyPortfolio",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    preferedAcousticness = table.Column<float>(type: "real", nullable: false),
                    preferedDanceability = table.Column<float>(type: "real", nullable: false),
                    preferedEnergy = table.Column<float>(type: "real", nullable: false),
                    preferedValence = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpotifyPortfolio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SimpleTrack",
                columns: table => new
                {
                    id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    SpotifyPortfolioId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    durationMs = table.Column<int>(type: "int", nullable: false),
                    href = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    previewUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    uri = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SimpleTrack", x => new { x.SpotifyPortfolioId, x.id });
                    table.ForeignKey(
                        name: "FK_SimpleTrack_SpotifyPortfolio_SpotifyPortfolioId",
                        column: x => x.SpotifyPortfolioId,
                        principalTable: "SpotifyPortfolio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SpotifyProfiles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AccessToken_access_token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccessToken_token_type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccessToken_scope = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccessToken_expires_in = table.Column<int>(type: "int", nullable: true),
                    AccessToken_expiryDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    AccessToken_refresh_token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PortfolioId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpotifyProfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SpotifyProfiles_SpotifyPortfolio_PortfolioId",
                        column: x => x.PortfolioId,
                        principalTable: "SpotifyPortfolio",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_SpotifyProfiles_PortfolioId",
                table: "SpotifyProfiles",
                column: "PortfolioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SimpleTrack");

            migrationBuilder.DropTable(
                name: "SpotifyProfiles");

            migrationBuilder.DropTable(
                name: "SpotifyPortfolio");
        }
    }
}
