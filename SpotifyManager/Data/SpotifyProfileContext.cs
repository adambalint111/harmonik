﻿using Microsoft.EntityFrameworkCore;
using SpotifyManager.Model;

namespace SpotifyManager.Data
{
    public class SpotifyProfileContext : DbContext
    {
        public DbSet<SpotifyProfile> SpotifyProfiles { get; set; }

        public SpotifyProfileContext(DbContextOptions<SpotifyProfileContext> options) : base(options)
        {

        }
    }
}
