﻿using Microsoft.EntityFrameworkCore;

namespace SpotifyManager.Model
{
    [Owned]
    public class SimpleTrack
    {
        public int durationMs
        {
            get;
            set;
        }

        public string? href
        {
            get;
            set;
        }

        public string? id
        {
            get;
            set;
        }

        public string? name
        {
            get;
            set;
        }

        public string? previewUrl
        {
            get;
            set;
        }

        public string? uri
        {
            get;
            set;
        }
    }
}
