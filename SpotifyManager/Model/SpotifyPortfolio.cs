﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpotifyManager.Model
{
    public class SpotifyPortfolio
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        public List<SimpleTrack> favouriteTracks { get; set; }

        public float preferedAcousticness { get; set; }

        public float preferedDanceability { get; set; }

        public float preferedEnergy { get; set; }

        public float preferedValence { get; set; }
    }
}
