﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpotifyManager.Model
{
    public class SpotifyProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        [Required]
        public string UserId { get; set; }

        public SpotifyToken? AccessToken { get; set; }

        public SpotifyPortfolio? Portfolio { get; set; }
    }
}
