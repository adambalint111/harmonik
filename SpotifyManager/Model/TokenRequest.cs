﻿using System.ComponentModel.DataAnnotations;

namespace SpotifyManager.Model
{
    public class TokenRequest
    {
        [Required]
        public string grant_type { get; set; }
        
        [Required]
        public string code { get; set; }
        
        [Required]
        public string redirect_uri { get; set; }
    }
}
