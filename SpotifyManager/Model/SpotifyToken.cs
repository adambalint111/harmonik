﻿using Microsoft.EntityFrameworkCore;

namespace SpotifyManager.Model
{
    [Owned]
    public class SpotifyToken
    {
        public string access_token { get; set; }

        public string token_type { get; set; }

        public string scope { get; set; }

        public int expires_in { get; set; }

        public DateTime expiryDate { get; set; }

        public string refresh_token { get; set; }
    }
}
