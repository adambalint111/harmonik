﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using SpotifyManager.Model;
using System.Text;
using System.Text.Json.Serialization;
using SpotifyAPI.Web;
using SpotifyManager.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace SpotifyManager.Controllers
{
    [ApiController]
    [Route("/api/spotify")]
    public class SpotifyRequestsController : ControllerBase
    {
        private string clientId;
        private string clientSecret;
        private string redirectUri;
        private RestClient spotifyAuthClient;
        private string spotifyBaseUri = @"https://accounts.spotify.com/";
        private SpotifyProfileContext context;

        public SpotifyRequestsController(IConfiguration configuration, SpotifyProfileContext context)
        {
            clientId = configuration["Spotify:ClientId"];
            clientSecret = configuration["Spotify:ClientSecret"];
            redirectUri = @"http://localhost:5083/callback";
            spotifyAuthClient = new RestClient(new HttpClientHandler() { AllowAutoRedirect = true });
            this.context = context;

            try
            {
                context.Database.EnsureCreated();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [HttpGet("profiles")]
        public IActionResult GetProfiles()
        {
            var profiles = context.SpotifyProfiles.Include(p => p.Portfolio).ToList();
            return Ok(profiles);
        }

        [HttpGet("profile")]
        public async Task<SpotifyProfile> GetSpotifyProfileByUserIdPublic([FromQuery]string userId)
        {
            if (userId == null)
            {
                return null;
            }

            var profile = await context.SpotifyProfiles.Include(p => p.Portfolio).FirstOrDefaultAsync(x => x.UserId == userId);

            return profile;
        }

        [HttpGet("login")]
        public async Task<IActionResult> LoginSpotify([FromQuery] string userId)
        {
            var query = QueryString.Create(new List<KeyValuePair<string, string?>>
            {
                new KeyValuePair<string, string?>("response_type", "code"),
                new KeyValuePair<string, string?>("client_id", clientId),
                new KeyValuePair<string, string?>("scope", "user-read-private user-read-email user-top-read"),
                new KeyValuePair<string, string?>("redirect_uri", redirectUri)
            });

            var spotifyProfile = await context.SpotifyProfiles.FirstOrDefaultAsync(x => x.UserId == userId);

            if (spotifyProfile == null)
            {
                await context.SpotifyProfiles.AddAsync(new SpotifyProfile() { UserId = userId });
                await context.SaveChangesAsync();
            }

            return Ok($"{spotifyBaseUri}authorize{query.ToUriComponent()}");
        }

        [HttpGet("portfolio/generate")]
        public async Task<IActionResult> GeneratePortfolio([FromQuery] string userId)
        {
            var spotifyProfile = await context.SpotifyProfiles.Include(sp => sp.AccessToken).Include(sp => sp.Portfolio).FirstOrDefaultAsync(x => x.UserId == userId);

            if (spotifyProfile == null || spotifyProfile.AccessToken == null || spotifyProfile.AccessToken.access_token == null)
            {
                return NotFound();
            }

            await ValidateToken(spotifyProfile);

            var spotifyApiClient = new SpotifyClient(spotifyProfile.AccessToken.access_token);
            List<FullTrack> favouriteTracksFull = null;
            List<TrackAudioFeatures> trackFeatures = null;
            try
            {
                favouriteTracksFull = (await spotifyApiClient.Personalization.GetTopTracks(new PersonalizationTopRequest() { Limit = 10, TimeRangeParam = PersonalizationTopRequest.TimeRange.MediumTerm })).Items;
                trackFeatures = (await spotifyApiClient.Tracks.GetSeveralAudioFeatures(new TracksAudioFeaturesRequest(new List<string>(favouriteTracksFull.Select(t => t.Id))))).AudioFeatures;
            } 
            catch(APIException e) 
            {
                // Prints: invalid id
                Console.WriteLine(e.Message);
                // Prints: BadRequest
                Console.WriteLine(e.Response?.StatusCode);
            }

            var favouriteTracksSimple = favouriteTracksFull?.Select(t => new Model.SimpleTrack() { durationMs = t.DurationMs, href = t.Href, id = t.Id, name = t.Name, previewUrl = t.PreviewUrl, uri = t.Uri }).ToList();
            var portfolio = new SpotifyPortfolio()
            {
                favouriteTracks = favouriteTracksSimple ?? new List<Model.SimpleTrack>(),
                preferedAcousticness = trackFeatures.Average(tf => tf.Acousticness),
                preferedDanceability = trackFeatures.Average(tf => tf.Danceability),
                preferedEnergy = trackFeatures.Average(tf => tf.Energy),
                preferedValence = trackFeatures.Average(tf => tf.Valence)
            };

            spotifyProfile.Portfolio = portfolio;
            await context.SaveChangesAsync();

            return Ok(portfolio);
        }

        [HttpGet("portfolio/mock-generate")]
        public async Task<IActionResult> GenerateMockPortfolio([FromQuery] string userId)
        {
            var spotifyProfile = await context.SpotifyProfiles.FirstOrDefaultAsync(x => x.UserId == userId);

            if (spotifyProfile == null)
            {
                await context.SpotifyProfiles.AddAsync(new SpotifyProfile() { UserId = userId });
                await context.SaveChangesAsync();

                spotifyProfile = await context.SpotifyProfiles.FirstOrDefaultAsync(x => x.UserId == userId);
            }

            List<Model.SimpleTrack> favouriteTracksSimple = new List<Model.SimpleTrack>();
            for (int i = 0; i < 10; i++)
            {
                favouriteTracksSimple.Add(new Model.SimpleTrack() { 
                    durationMs = 100, 
                    href = @"https://open.spotify.com/track/6svmDWH7XmHQWGTXlTE58I?si=1f11468fe0a24a27", 
                    id = Guid.NewGuid().ToString(), 
                    name = "song", 
                    previewUrl = @"https://open.spotify.com/track/6svmDWH7XmHQWGTXlTE58I?si=1f11468fe0a24a27", 
                    uri = @"https://open.spotify.com/track/6svmDWH7XmHQWGTXlTE58I?si=1f11468fe0a24a27" 
                });
            }

            Random rand = new();
            var portfolio = new SpotifyPortfolio()
            {
                favouriteTracks = favouriteTracksSimple,
                preferedAcousticness = (float)rand.NextDouble(),
                preferedDanceability = (float)rand.NextDouble(),
                preferedEnergy = (float)rand.NextDouble(),
                preferedValence = (float)rand.NextDouble()
            };

            spotifyProfile.Portfolio = portfolio;
            await context.SaveChangesAsync();

            return Ok(spotifyProfile);
        }

        [HttpGet("portfolio/refresh")]
        public IActionResult RefreshPortfolio()
        {
            return Ok();
        }

        [HttpGet("callback")]
        public async Task<ActionResult<SpotifyToken>> Callback([FromQuery] string userId, [FromQuery] string? code, [FromQuery] string? state)
        {
            if (code == null)
            {
                return BadRequest("Something wrong");
            }

            TokenRequest requestBody = new TokenRequest()
            {
                code = code,
                grant_type = "authorization_code",
                redirect_uri = redirectUri
            };

            var requestForToken = new RestRequest($"{spotifyBaseUri}api/token", Method.Post);
            requestForToken.AddHeader("Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}"))}");
            requestForToken.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            requestForToken.AddParameter("grant_type", requestBody.grant_type);
            requestForToken.AddParameter("code", requestBody.code);
            requestForToken.AddParameter("redirect_uri", requestBody.redirect_uri);
            
            var response = await spotifyAuthClient.ExecuteAsync(requestForToken);
            SpotifyToken token = JsonConvert.DeserializeObject<SpotifyToken>(response.Content);
            token.expiryDate = DateTime.Now + TimeSpan.FromSeconds(token.expires_in);

            try
            {
                await SaveToken(token, userId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [HttpGet("similar-portfolios")]
        public async Task<IActionResult> GetSimilarPortfolios([FromQuery] string userId, [FromQuery] string? skipIds = null)
        {
            var spotifyProfile = await GetSpotifyProfileByUserId(userId);
            
            if (spotifyProfile == null)
            {
                return NotFound();
            }

            var favouriteTracks = spotifyProfile.Portfolio.favouriteTracks.Select(ft => ft.id);

            var skipIdsArray = skipIds?.Split(',') ?? Array.Empty<string>();

            // TODO: Cache calculated ratings
            var candidateProfiles = await context.SpotifyProfiles
                .Include(p => p.Portfolio)
                .Where(sp => sp != spotifyProfile)
                .Where(sp => !skipIdsArray.Contains(sp.UserId))
                .Where(sp => sp.Portfolio != null)
                .ToListAsync();

            var candidates = candidateProfiles
                .Select(sp => new
                {
                    UserId = sp.UserId,
                    MatchRate = Math.Round(1 - ((Math.Abs(sp.Portfolio.preferedEnergy - spotifyProfile.Portfolio.preferedEnergy)
                            + Math.Abs(sp.Portfolio.preferedValence - spotifyProfile.Portfolio.preferedValence)
                            + Math.Abs(sp.Portfolio.preferedAcousticness - spotifyProfile.Portfolio.preferedAcousticness)
                            + Math.Abs(sp.Portfolio.preferedDanceability - spotifyProfile.Portfolio.preferedDanceability)) / 4.0), 4),
                    SharedFavourites = sp.Portfolio.favouriteTracks.Count(ft => favouriteTracks.Contains(ft.id))
                })
                .OrderByDescending(x => x.SharedFavourites)
                .ThenByDescending(x => x.MatchRate)
                .Take(3);

            return Ok(candidates.ToList());
        }

        private async Task SaveToken(SpotifyToken token, string userId)
        {
            var spotifyProfile = await GetSpotifyProfileByUserId(userId);

            if (spotifyProfile == null)
            {
                throw new ArgumentException(userId);
            }

            spotifyProfile.AccessToken = token;
            await context.SaveChangesAsync();
        }

        private async Task ValidateToken(SpotifyProfile profile)
        {
            if (profile.AccessToken.expiryDate.CompareTo(DateTime.Now) <= 0)
            {
                var refreshedToken = await GetRefreshedToken(profile.AccessToken);
                profile.AccessToken = refreshedToken;
                await context.SaveChangesAsync();
            }
        }

        private async Task<SpotifyToken> GetRefreshedToken(SpotifyToken token)
        {
            var requestForTokenRefresh = new RestRequest($"{spotifyBaseUri}api/token", Method.Post);
            requestForTokenRefresh.AddHeader("Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}"))}");
            requestForTokenRefresh.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            requestForTokenRefresh.AddParameter("grant_type", "refresh_token");
            requestForTokenRefresh.AddParameter("refresh_token", token.refresh_token);

            var response = await spotifyAuthClient.ExecuteAsync(requestForTokenRefresh);
            SpotifyToken refreshedToken = JsonConvert.DeserializeObject<SpotifyToken>(response.Content);
            refreshedToken.expiryDate = DateTime.Now + TimeSpan.FromSeconds(refreshedToken.expires_in);

            return refreshedToken;
        }

        private async Task<SpotifyProfile> GetSpotifyProfileByUserId(string userId)
        {
            if (userId == null)
            {
                return null;
            }

            return await context.SpotifyProfiles.Include(p => p.Portfolio).Include(p => p.AccessToken).FirstOrDefaultAsync(x => x.UserId == userId);
        }
    }
}
